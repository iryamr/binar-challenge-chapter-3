package id.binar.challenge.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Person(
    val name: String? = null,
    val age: Int = 0,
    val address: String? = null,
    val job: String? = null,
) : Parcelable
