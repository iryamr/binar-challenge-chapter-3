package id.binar.challenge.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import id.binar.challenge.R
import id.binar.challenge.databinding.FragmentSecondBinding
import id.binar.challenge.model.Person

class SecondFragment : Fragment(R.layout.fragment_second) {

    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSecondBinding.bind(view)

        binding.etName.requestFocus()

        binding.btnMoveToScreenThree.setOnClickListener {
            val name = binding.etName.text.toString()

            if (name.isEmpty()) {
                binding.etName.error = "Please input your name"
                return@setOnClickListener
            }

            val person = Person(name = name)

            val toThirdFragment =
                SecondFragmentDirections.actionSecondFragmentToThirdFragment(person)

            it.findNavController().navigate(toThirdFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}