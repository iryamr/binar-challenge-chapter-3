package id.binar.challenge.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.findNavController
import id.binar.challenge.R
import id.binar.challenge.databinding.FragmentThirdBinding
import id.binar.challenge.model.Person

@SuppressLint("SetTextI18n")
class ThirdFragment : Fragment(R.layout.fragment_third) {

    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(FourthFragment.REQUEST_KEY) { _, bundle ->
            val person = bundle.getParcelable<Person>(FourthFragment.EXTRA_PERSON) as Person

            val oddOrEvenAge = if (person.age % 2 == 0) "Genap" else "Ganjil"

            binding.apply {
                tvAge.isVisible = true
                tvAge.text = "Umur Anda ${person.age}, bernilai $oddOrEvenAge"

                tvAddress.isVisible = true
                tvAddress.text = "Alamat Anda ${person.address}"

                tvJob.isVisible = true
                tvJob.text = "Pekerjaan Anda ${person.job}"
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentThirdBinding.bind(view)

        val person = ThirdFragmentArgs.fromBundle(arguments as Bundle).person
        binding.tvName.text = "Nama Anda ${person.name}"

        binding.btnMoveToScreenFour.setOnClickListener {
            val toFourthFragment = ThirdFragmentDirections.actionThirdFragmentToFourthFragment()

            it.findNavController().navigate(toFourthFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}