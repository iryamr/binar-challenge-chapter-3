package id.binar.challenge.ui

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import id.binar.challenge.R
import id.binar.challenge.databinding.FragmentFourthBinding
import id.binar.challenge.model.Person

class FourthFragment : Fragment(R.layout.fragment_fourth) {

    private var _binding: FragmentFourthBinding? = null
    private val binding get() = _binding!!

    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            setResult()
            requireActivity().findNavController(R.id.nav_host_fragment).popBackStack()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, backPressedCallback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentFourthBinding.bind(view)

        binding.etAge.requestFocus()

        binding.btnBackToScreenThree.setOnClickListener {
            val age = binding.etAge.text.toString()
            val address = binding.etAddress.text.toString()
            val job = binding.etJob.text.toString()

            if (age.isEmpty()) {
                binding.etAge.error = "Please input your age"
                return@setOnClickListener
            }

            if (address.isEmpty()) {
                binding.etAddress.error = "Please input your address"
                return@setOnClickListener
            }

            if (job.isEmpty()) {
                binding.etJob.error = "Please input your job"
                return@setOnClickListener
            }

            val person = Person(
                age = age.toInt(),
                address = address,
                job = job
            )

            val bundle = Bundle().apply {
                putParcelable(EXTRA_PERSON, person)
            }

            setFragmentResult(REQUEST_KEY, bundle)

            it.findNavController().popBackStack()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setResult() {
        val age = binding.etAge.text.toString()
        val address = binding.etAddress.text.toString()
        val job = binding.etJob.text.toString()

        if (age.isEmpty() && address.isEmpty() && job.isEmpty()) {
            return
        }

        val person = Person(
            age = age.toInt(),
            address = address,
            job = job
        )

        val bundle = Bundle().apply {
            putParcelable(EXTRA_PERSON, person)
        }

        setFragmentResult(REQUEST_KEY, bundle)
    }

    companion object {
        const val EXTRA_PERSON = "extra_person"
        const val REQUEST_KEY = "request_key"
    }
}